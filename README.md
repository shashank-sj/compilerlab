# CompilerLab

## Cycle 1

- [x] Lexical analyzer in C
- [x] Find e-closure of NFA

## Cycle 2
- [ ] Operator Precedent Parser
- [ ] First and Follow of a Grammer
- [ ] Recursive descent parser
- [ ] Shift Reduce Parser   