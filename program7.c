#include <stdio.h>
#include <string.h>
#define max 20

char ter[max], ip[max], op[max][max][1], stack[max];
void clear()
{
  for (int i = 0; i < max; i++)
  {
    stack[i] = NULL;
    ip[i] = NULL;
    for (int j = 0; j < max; j++)
      op[i][j][1] = NULL;
  }
}
void main()
{
  int n, i, j, k, top = 0, row, col;
  clear();
  printf("enetr no.of terminals:");
  scanf("%d", &n);
  printf("enter the terminals: ");
  scanf("%s", ter);
  printf("Enter the values to table\n\tOperator Precedence Table\n\t");
  for (int i = 0; i < n; i++)
  {
    printf("\t %c", ter[i]);
  }
  for (int i = 0; i < n; i++)
  {
    printf("\n\t %c\t", ter[i]);
    for (int j = 0; j < n; j++)
    {
      scanf("%s", op[i][j]);
    }
  }
  stack[top] = '$';
  printf("Enter the input string: ");
  scanf("%s", ip);
  strcat(ip,"$");
  i = 0;
  printf("\n STACK\t\t Input String\t\t Action\n");
  printf("\n%s\t\t%s\t\t", stack, ip);
  while (i <= strlen(ip))
  {
    for (int k = 0; k < n; k++)
    {
      if (stack[top] == ter[k])
        col = k;
      if (ip[i] == ter[k])
        row = k;
    }
    if ((stack[top] == '$') && (ip[i] == '$'))
    {
      printf("String is accepted\n");
      break;
    }
    else if ((op[col][row][0] == '<') || (op[col][row][0] == '='))
    {
      stack[++top] = op[col][row][0];
      stack[++top] = ip[i];
      printf("Shift %c", ip[i]);
      i++;
    }
    else
    {
      if (op[col][row][0] == '>')
      {
        while (stack[top] != '<')
        {
          --top;
        }
        top -= 1;
        printf("Reduce");
      }
      else
      {
        printf("\nString is rejected\n");
        break;
      }
    }
    printf("\n");
    for (k = 0; k <= top; k++)
    {
      printf("%c", stack[k]);
    }
    printf("\t\t");
    for (k = i; k < strlen(ip); k++)
    {
      printf("%c", ip[k]);
    }
    printf("\t\t");
  }
}
