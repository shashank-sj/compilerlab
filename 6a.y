%{
#include<stdio.h>
int valid = 1;
int yylex();
void yyerror(char *s);
%}
%token num id op
%%

start : id '=' s ';'
        | s ';'
        | s

s :     id x

      | num x

      | '-' num x

      | '(' s ')' x

      ;

x :     op s

      | '-' s

      |

      ;

%%
int main()
{
        yyparse();
        if(valid)
            printf("Valid expression\n");
        return 0;
}
void yyerror(char *s)
{
    valid = 0;
    printf("Invalid expression\n");
}
