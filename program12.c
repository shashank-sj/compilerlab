#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define max 50
void main()
{
    FILE *fi, *fo;
    fi = fopen("loopinput.c", "r");
    fo = fopen("loopoutput.c", "w");
    char data[max], d;
    int f = 0, tf = -1, l;
    while ((d = getc(fi)) != EOF)
    {
        if (d == '\n' || d == ' ' || d== '#')
            fprintf(fo, "%c", d);
        fscanf(fi, "%s", data);
        if (strcmp(data, "for") == 0){
            l = f + 8;
            tf = 0;
        }
        if (tf == 0 && f == l){
            int val = atoi(data);
            val = val / 2;
            sprintf(data, "%d", val);
            tf++;
            l += 4;
        }
        else if (tf == 1 && f == l){
            int val = atoi(data);
            val = val + 1;
            sprintf(data, "%d", val);
            tf++;
            l += 2;
        }
        else if (tf == 2 && f == l){
            fprintf(fo, "%s\n", data);
            tf = 0;
            l = -1;
        }
        fprintf(fo, "%s", data);
        f++;
    }
}