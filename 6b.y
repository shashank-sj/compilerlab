%{
    #include<stdio.h>
    int valid=1;
    int yylex();
    int yyerror();
%}

%token digit letter

%%
start : letter s
s :     letter s
      | digit s
      |
      ;
%%
int yyerror(){
    printf("Its not a valid identifier!\n");
    valid=0;
    return 0;
}
int main(){
    printf("\nEnter a variable:");
    yyparse();
    if(valid){
        printf("It is a identifier!\n");
    }
}
