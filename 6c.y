%{
#include<stdio.h>
int yylex();
void yyerror(char *s);
%}
%token NUMBER
%left '+' '-'
%left '*' '/'
%%
st: exp {printf("Result ::%d\n",$$);}
    ;
exp: exp '+' exp {$$ = $1 + $3;}
    |exp '-' exp {$$ = $1 - $3;}
    |exp '*' exp {$$ = $1 * $3;}
    |exp '/' exp {$$ = $1 / $3;}
    |'('exp')' {$$ = $2;}                                   
    |NUMBER {$$ = $1;}
    ;
%%
int main()
{
        yyparse();
        return 0;
}
void yyerror(char *s)
{
    printf("error: %s\n",s);
}
