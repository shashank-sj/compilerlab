#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define LEXEME_LEN 20

//globals
char *symbols[] = {"{", "}", "(", ")", ";"};
char *operators[] = {">", "<", "=", "==", ">=", "<=", "+", "-", "*", "/","++","--"};
char *keywords[] = {"auto", "break", "case", "char", "const", "continue", "default",
                    "do", "double", "else", "enum", "extern", "float", "for", "goto",
                    "if", "int", "long", "register", "`", "short", "signed",
                    "sizeof", "static", "struct", "switch", "typedef", "union",
                    "unsigned", "void", "volatile", "while"};
int in_str_list(char *word, char *list[])
{
    int i = 0;
    while (list[i])
    {
        if (!strcmp(list[i], word))
            return 1;
        i++;
    }
    return 0;
}
int isOperator(char ch)
{
    if (ch == '+' || ch == '-' || ch == '*' ||
		ch == '/' || ch == '>' || ch == '<' ||
		ch == '=' || ch == '%')
        return 1;
    return 0;
}
int isDelimiter(char ch)
{
    if (ch == ' ' || ch == ',' || ch == ';' || ch == '(' || ch == ')' ||
        ch == '[' || ch == ']' || ch == '{' || ch == '}' || ch == '"' || 
        isOperator(ch))
        return 1;
    return 0;
}
int is_keyword(char *lexeme)
{
    return in_str_list(lexeme, keywords);
}
int is_operator(char *lexeme)
{
    return in_str_list(lexeme, operators);
}
int is_constant(char *lexeme)
{
    int i, is_digit; //number constant
    for (i = 0; lexeme[i] != '\0'; ++i)
    {
        is_digit = 1;
        if (!isdigit((int)lexeme[i]))
        {
            is_digit = 0;
            break;
        }
    }
    return is_digit;
}
int is_symbol(char *lexeme)
{
    return in_str_list(lexeme, symbols);
}
int is_identifier(char *lexeme)
{
    if (lexeme[0] == '_')
        return 1;
    if
        ispunct((lexeme[0])) return 0;
    if
        isdigit((lexeme[0])) return 0;

    return 1;
}
int main(int argc, char *argv[]) //input file is taken as command line arg
{
    FILE *fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("File not found!\n");
        return 1;
    }
    int i = 0;
    char c, lexeme[LEXEME_LEN];
    while ((c = getc(fp)) != EOF)
    {
        if (c == '#')
        {
            do
            {
                printf("%c", c);
            } while ((c = getc(fp)) != '\n');
            printf("\t is a preprocessor\n");
        }
        else if (c == '/')
        {
            if (c = getc(fp) == '/') //single line comment
            {
                while ((c = getc(fp)) != '\n')
                {
                    printf("%c", c);
                }
            }
            else if (c = getc(fp) == '*') //multi line comment
            {
                while ((c = getc(fp)) != '/')
                {
                    printf("%c", c);
                }
            }
            printf("\t is a comment\n");
        }
        if(c== '"'){
            while ((c = getc(fp))!='"')
            {
                printf("%c",c);
            }
            printf(" is a string\n");
		}
        else if(isOperator(c)){
                lexeme[i++]=c;
                c=getc(fp);
                if(isOperator(c))
                    lexeme[i++]=c;
                else
                    ungetc(c, fp);
                lexeme[i]='\0';i=0;
                if(is_operator(lexeme))
                    printf("%s \t Operator\n", lexeme);
                else
                    printf("Invalid operator");
        }
        else if(isDelimiter(c)&&c!=' ')
            printf("%c \t Symbol\n", c);
        else if (isalnum(c))
        {
            lexeme[i++] = c;
            while (c=getc(fp))
            {
                if(isDelimiter(c))
                    break;
                lexeme[i++] = c;
            }
            ungetc(c, fp);
            lexeme[i] = '\0';i=0;
            if (is_keyword(lexeme))
                printf("%s \t Keyword\n", lexeme);
            else if (is_constant(lexeme))
                printf("%s \t Constant\n", lexeme);
            else if (is_symbol(lexeme))
                printf("%s \t Symbol\n", lexeme);
            else if (is_identifier(lexeme))
                printf("%s \t Identifier\n", lexeme);
            else
                printf("%s \t Invalid Identifier\n", lexeme);
        }
        else if (c == ' ' || c == '\t' || c == '\n') // ignore redundant spaces, tabs and new lines
            ;
    }
    return 0;
}